var express = require('express');
var app = express();
var http = require('http');
var retool = require('@retool/retool');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');


// view engine setup


app.set('views', path.join(__dirname, 'pages'));
app.set('view engine', 'pug');

app.use(logger('dev'));

app.use(favicon(__dirname + '/public/favicon.ico'));

app.use(retool.assets());

if (process.env.RETOOL_MODE == "dev") {
	app.use(retool.ide());
}
else {
	app.use(retool.app());
}
app.use("/assets/",express.static(path.join(__dirname, 'public')));


app.use("/api/hello",function(req,res){
	res.send("Hello");
});


// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

/*
var server = http.createServer(app);
var port = process.env.PORT || 3000;
server.listen(port,function(){
	console.log("server listening on port=" + port);
});
*/

app.set('port', process.env.PORT);
app.listen(app.get('port'), function() {
    console.log('Listening on port ' + app.get('port'));
});
